meters = 640
inches = meters/0.0254 
feets = inches/12
yards = feets/3
miles = yards/1760

print """
%g meters are equivalent to
%g inches,
%g feets,
%g yards, and
%g miles
""" % (meters, inches, feets, yards, miles)
