#from math import sin, cos
from math import sin, cos, pi
x = pi/4 # needs to import pi
#1_val = sin^2(x) + cos^2(x) # error : 1_val is illegal, ^ syntax incorrect
val = sin(x)**2 + cos(x)**2 # error : 1_val is illegal, ^ does not exist
#print 1_VAL # error: is 1_val
print val # error: is 1_val

