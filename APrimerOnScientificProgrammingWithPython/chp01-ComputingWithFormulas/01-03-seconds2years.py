seconds = 1.0e9
years = seconds/(365*24*60*60.0)

print "%g seconds are equivalent to %g years" % (seconds, years)
