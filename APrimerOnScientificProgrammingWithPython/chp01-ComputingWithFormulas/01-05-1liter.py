# I will use advanced functions. This is more than the asked solution

import sys
ARGC = len(sys.argv)

def usage():
    print "\nUsage:"
    print "%s filename" % (sys.argv[0])

# make sure the filename was given
if ARGC != 2:
    usage()
    sys.exit(1)

try :
    IFILE = open(sys.argv[1])
except :
    print "Filename %s DOES NOT EXISTS" % (sys.argv[1])

for line in IFILE.readlines() :
    tokens = line.split()
    name = tokens[:-1]
    val = float(str(tokens[-1]))
    print "The mass for one liter of  %s  is -> %g" % (line.replace(tokens[-1], ''), val)  # since 1 lt = 1000 cm^3

IFILE.close()

