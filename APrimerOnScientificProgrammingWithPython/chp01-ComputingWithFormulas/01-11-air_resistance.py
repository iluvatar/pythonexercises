import math as m

# define constants
CD    = 0.2
RHO   = 1.2 # kg/m^3
RAD   = 0.11 # m
A     = m.pi*RAD*RAD # m^2
MASS  = RHO*4.0*(RAD**3)/3.0 # kg
G     = 9.81 # m/s^2
FG    = MASS*G

V = 120*1000.0/3600
FD = 0.5*CD*RHO*A*V*V
print "For a velocity of %g m/s, the grav and drag forces and their ratio are %g %g %g" % (V, FG, FD, FD/FG)
V = 10*1000.0/3600
FD = 0.5*CD*RHO*A*V*V
print "For a velocity of %g m/s, the grav and drag forces and their ratio are %g %g %g" % (V, FG, FD, FD/FG)