C = 41 # Assign an int object holding 41 to C
print C == 40 # False  
print C != 40 and C < 41 # True and False = False 
print C!=40 or C<41 # True or False = True
print not C == 40 # not False = True
print not C > 40 # not True = False
print C <= 41 # True
print not False # True
print True and False # False
print False or True # True
print False or False or False # False 
print True and True and False # False
print False == 0 # True
print True == 0 # False
print True == 1 # True

