V0 = 10.0 # m/s
G = 9.81 # m/s^2
N = 81
DT = 2*(V0/G)/N
time = [DT*it for it in range(N)]
y = [V0*t - 0.5*G*t*t for t in time ]

for t, y in zip(time, y) :
    print "%25.16e\t%25.16e" % (t, y)
