from math import sqrt
for n in range(1, 50): # 60 is too much, less than precision
    print
    r = 2.0
    for i in range(n):
        r = sqrt(r)
    print "%25.16e" % (r)
    for i in range(n):
        r = r**2
    print "%25.16e" % (r)
    print '%d times sqrt and **2: %.16f' % (n, r)
