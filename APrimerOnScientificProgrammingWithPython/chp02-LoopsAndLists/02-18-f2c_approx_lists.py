F = []
C = []
C_approx = []

for f in range(0, 110, 10) :
    F.append(f)
    C.append((5.0/9.0)*(f-32))
    C_approx.append((f - 30)/2.0)

conversion = [ [f, c, c_approx] for f, c, c_approx in zip(F, C, C_approx) ]

for f, c, c_approx in zip(F, C, C_approx) :
    print "%5.2f\t%5.2f\t%5.2f" % (f, c, c_approx)
