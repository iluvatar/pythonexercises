# The code decreases a number until it is indistinguishable from 1
# More resolution can be obtained by not divinding by 2 but by a number in (1, 2)

eps = 1.0
while 1.0 != 1.0 + eps:
    print '...............', eps
    eps = eps/2.0
