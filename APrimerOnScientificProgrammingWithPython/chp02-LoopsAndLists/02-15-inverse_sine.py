import math as m

N   = 10
MIN = 0.0
MAX = 1.0
DX  = (MAX-MIN)/N

print "# %23s\t%25s" % ("x", "asin(x)")
for ii in range(N) :
    x = MIN + ii*DX
    print "%25.16e\t%25.16e" % (x, m.asin(x))
    