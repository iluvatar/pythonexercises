V0 = 10.0 # m/s
G = 9.81 # m/s^2
N = 81
DT = 2*(V0/G)/N

time = [DT*it for it in range(N)]
y = [V0*t - 0.5*G*t*t for t in time ]

ty1 = [time, y]
for ii in range(len(ty1[0])) :
    print "%25.16e\t%25.16e" % (ty1[0][ii], ty1[1][ii])
print

ty2 = [ [t, h] for t,h in zip(time, y) ] 
for t, h in ty2 :
    print "%25.16e\t%25.16e" % (t, h)
print
