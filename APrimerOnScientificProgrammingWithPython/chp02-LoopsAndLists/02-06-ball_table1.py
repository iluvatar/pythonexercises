V0 = 1.0 # m/s
G = 9.81 # m/s^2
N = 11
DT = 2*(V0/G)/N

for it in range(N) :
    t = DT*it
    y = V0*t - 0.5*G*t*t
    print "%25.16e\t%25.16e" % (t, y)
