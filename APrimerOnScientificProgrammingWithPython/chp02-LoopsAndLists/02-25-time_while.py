# this code sleeps prints 5 times the message '... I like ...' every 2 seconds
# < is changed to > , then the code waits for 10 seconds and starts printing the same message every 2 seconds, for ever
import time
t0 = time.time()
while time.time() - t0 < 10:
    print '....I like while loops!'
    time.sleep(2)
print 'Oh, no - the loop is over.'
